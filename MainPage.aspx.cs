﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

/* this is the class for the movie objects. Each movie has a number of mood atributes, along with
   getters and setters. The atributes are set by the XML file. Since it's the only custom class
   needed in this program, I didn't put it in a seperate file */
public class Movie
{
    private string name, imagePath;
    private int happiness, calmness, complexity, scariness, appropriateness;

    // initialiser sets default values, which will be overwritten by the XML data
    public Movie()
    {
        name = "No Content";
        imagePath = "~/Images/DefaultPoster";
        happiness = 3;
        calmness = 3;
        complexity = 3;
        scariness = 3;
        appropriateness = 8;
    }

    public string getName() { return name; }
    public string getImagePath() { return imagePath; }
    public int getMood(string m)
    {
        if (m == "happiness") return happiness;
        if (m == "calmness") return calmness;
        if (m == "complexity") return complexity;
        if (m == "scariness") return scariness;
        return 3;
    }
    public int getAppropriateness() { return appropriateness; }

    public void setName(string n) { name = n; }
    public void setImagePath(string p) { imagePath = p; }
    public void setMood(string m, int v)
    {
        if (m == "happiness") happiness = v;
        if (m == "calmness") calmness = v;
        if (m == "complexity") complexity = v;
        if (m == "scariness") scariness = v;
    }
    public void setAppropriateness(int a) { appropriateness = a; }
};

public partial class MainPage : System.Web.UI.Page
{
    /* this is the list of movie objects. I used list instead of array, as the number of movies is determined by the XML file.
       the listPopulated bool is to ensure that the list is only filled once */
    public List<Movie> movieList = new List<Movie>();
    public bool listPopulated = false;
    public TextBox[] boundControls = new TextBox[4];

    // called when the page first loads
    protected void Page_Load(object sender, EventArgs e)
    {
        // save the slider value boxes into an array, as they are needed a couple of times
        boundControls[0] = Slider1_BoundControl;
        boundControls[1] = Slider2_BoundControl;
        boundControls[2] = Slider3_BoundControl;
        boundControls[3] = Slider4_BoundControl;

        // hide the slider value boxes. Can't set to "visible = false" or the values become inaccesible
        for (int i = 0; i < 4; i++)
        {
            boundControls[i].Style.Add("display", "none");
        }
    }

    /* called when the user clicks the Upload button. Standard file upload function, which checks to ensure file
       is properly named and in XML format. Tells the user about any problems, otherwise saves the file.
       Note: The upload will overwrite the CustomList.xml file, but the default XML file cannot be changed */
    protected void Btn_Upload_Click(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            Boolean fileOK = false;
            String path = Server.MapPath("~/Data/");
            if (FileUpload1.FileName == "CustomList.xml")
            {
                if (FileUpload1.HasFile)
                {
                    String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    if (fileExtension == ".xml") fileOK = true;
                }
            }

            if (fileOK)
            {
                try
                {
                    FileUpload1.PostedFile.SaveAs(path + FileUpload1.FileName);
                    Lbl_UploadState.Text = "File uploaded!";
                }
                catch (Exception ex)
                {
                    Lbl_UploadState.Text = "File could not be uploaded: " + ex.Message;
                }
            }
            else
            {
                Lbl_UploadState.Text = "Cannot accept this file. Ensure your file is called \"CustomList.xml\"";
            }
        }
    }

    /* this function is called when the user presses one of the Update buttons, either default or custom
       It parses the XML file, looking for specific tags, and populates the movieList with movie objects, filling in 
       their attributes as it goes. The custom file has already been checked by now, so there shouldn't be any exceptions */
    void PopulateList(bool custom)
    {
        string path;
        if (listPopulated) return;
        if (custom) path = Server.MapPath("~/Data/CustomList.xml");
        else path = Server.MapPath("~/Data/MovieList.xml");

        XmlDocument doc = new XmlDocument();
        doc.Load(path);

        int movieNumber = 0;

        foreach (XmlNode node in doc.DocumentElement.ChildNodes)
        {
            movieList.Add(new Movie());

            foreach (XmlNode innerNode in node.ChildNodes)
           {
                /* set the values using data from the XML file. Note: it attempts to parse the values
                   but shouldn't throw an exception if it fails */
                if (innerNode.Name == "name") movieList[movieNumber].setName(innerNode.InnerText);
                if (innerNode.Name == "imagepath") movieList[movieNumber].setImagePath(innerNode.InnerText);
                if (innerNode.Name == "happiness") SetMood(innerNode, "happiness", movieNumber);
                if (innerNode.Name == "calmness") SetMood(innerNode, "calmness", movieNumber);
                if (innerNode.Name == "complexity") SetMood(innerNode, "complexity", movieNumber);
                if (innerNode.Name == "scariness") SetMood(innerNode, "scariness", movieNumber);
            }

            movieNumber++;
        }

        listPopulated = true;
    }

    // parses the vaules from the XML file and sets the various movie attributes
    void SetMood(XmlNode node, string moodName, int movNumber)
    {
        int num;
        if (Int32.TryParse(node.InnerText, out num)) movieList[movNumber].setMood(moodName, num);
    }

    // when the Update (default) button is presses
    protected void Btn_Update_Click(object sender, EventArgs e)
    {
        // populate the list with default XML values
        PopulateList(false);
        MovieSelector();
    }

    // when the Update (custom) button is pressed
    protected void Button_UpdateCustom_Click(object sender, EventArgs e)
    {
        // populate the list with custom XML values. Catches exceptions if the custom file isn't formatted correctly
        try
        {
            PopulateList(true);
            MovieSelector();
        }
        catch(Exception ex)
        {
            Lbl_UploadState.Text = "Problem with custom XML file: " + ex.Message;
        }
    }

    /* Okay, here's the fun part - the function to update the movie selection and refresh the poster images. There are 4
    sliders, each with a value from 1 to 5 (the first slider is "happiness", with 1 representing "Sad" and 5 being "Happy").
    Each movie object also has these 4 attribues, set from 1 to 5. We step through each, comparing the movie values to the 
    values of the sliders, and counting the difference. This gives us an "appropriateness" total for each movie (zero means
    the movie perfectly matches the slider values). The movie list is then reordered by appropriateness, and the poster images
    are updated with the top 5 movies - the left represents the best choice, the next one is the second best, and so on.
    Note: I went with an Update buttons so this function isn't called constantly while the user is setting the sliders. */
    void MovieSelector()
    {
        int runningTotal = 0, difference = 0;

        // go through all the movies, comparing mood values and ending up with a total appropriateness value
        foreach (Movie movie in movieList)
        {
            runningTotal = 0; // reset the total between movies

            // now step through the different mood values
            string moodName = "";

            for (int i = 0; i < 4; i++)
            {
                if (i == 0) moodName = "happiness";
                if (i == 1) moodName = "calmness";
                if (i == 2) moodName = "complexity";
                if (i == 3) moodName = "scariness";

                difference = Convert.ToInt32(boundControls[i].Text) - movie.getMood(moodName);
                if (difference < 0) difference = 0 - difference; // in case number is negative
                runningTotal += difference;
            }

            // now set the movie's total appropriateness
            movie.setAppropriateness(runningTotal);
        }

        // sort the movie list in order of appropriateness
        movieList.Sort(delegate (Movie x, Movie y)
        {
            return x.getAppropriateness().CompareTo(y.getAppropriateness());
        });

        /* finally, set the poster images and movie names. This could have been done with a loop, but all the
           poster images and name boxes would need to be stored in arrays, which would take more code than this, 
           and this is the only time they are accessed */
        Img_Poster1.ImageUrl = movieList[0].getImagePath();
        Lbl_MovieName1.Text = movieList[0].getName();
        Img_Poster2.ImageUrl = movieList[1].getImagePath();
        Lbl_MovieName2.Text = movieList[1].getName();
        Img_Poster3.ImageUrl = movieList[2].getImagePath();
        Lbl_MovieName3.Text = movieList[2].getName();
        Img_Poster4.ImageUrl = movieList[3].getImagePath();
        Lbl_MovieName4.Text = movieList[3].getName();
        Img_Poster5.ImageUrl = movieList[4].getImagePath();
        Lbl_MovieName5.Text = movieList[4].getName();
    }
}