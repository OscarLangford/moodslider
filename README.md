﻿MoodSlider is a simple web application that picks movies based on the user's mood.
It has a default XML file that includes the movie data, or the user can upload their
own, provided it is properly formatted. Created using Visual Studio and ASP.NET.

To try the app out, click [here](http://moodslider.azurewebsites.net/)