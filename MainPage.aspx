﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MainPage.aspx.cs" Inherits="MainPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
    
        <asp:Image ID="Img_SkyLogo" runat="server" Height="62px" Width="99px" src="Images/SkyLogo.jpg"/>
    
        <asp:Label ID="Lbl_Title" runat="server" Font-Size="50" Text="MoodSlider" style="z-index: 1; left: 178px; top: -2px; 
            position: absolute; margin-bottom: 0px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"></asp:Label>
    
        <asp:Label ID="Lbl_UploadState" runat="server" Height="20px" style="z-index: 1; left: 645px; top: 52px; position: absolute; width: 111px; margin-bottom: 0px" Text=" "></asp:Label>
    
    </div>
        <asp:Label ID="Lbl_Emotion1" runat="server" style="z-index: 1; left: 15px; top: 87px; position: absolute; height: 45px; width: 94px; 
            font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Sad" Font-Size="25pt"></asp:Label>
        <asp:Label ID="Lbl_Emotion2" runat="server" style="z-index: 1; left: 533px; top: 87px; position: absolute; height: 45px; width: 94px;
             font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Happy" Font-Size="25pt"></asp:Label>

        <asp:Label ID="Lbl_Emotion3" runat="server" style="z-index: 1; left: 15px; top: 130px; position: absolute; height: 45px; width: 94px; 
            font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Agitated" Font-Size="25pt"></asp:Label>
        <asp:Label ID="Lbl_Emotion4" runat="server" style="z-index: 1; left: 533px; top: 130px; position: absolute; height: 45px; width: 94px;
             font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Calm" Font-Size="25pt"></asp:Label>

        <asp:Label ID="Lbl_Emotion5" runat="server" style="z-index: 1; left: 15px; top: 173px; position: absolute; height: 45px; width: 94px; 
            font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Tired" Font-Size="25pt"></asp:Label>
        <asp:Label ID="Emotion6" runat="server" style="z-index: 1; left: 533px; top: 173px; position: absolute; height: 45px; width: 94px;
             font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Awake" Font-Size="25pt"></asp:Label>

        <asp:Label ID="Emotion7" runat="server" style="z-index: 1; left: 15px; top: 216px; position: absolute; height: 45px; width: 94px; 
            font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Scared" Font-Size="25pt"></asp:Label>
        <asp:Label ID="Emotion8" runat="server" style="z-index: 1; left: 533px; top: 216px; position: absolute; height: 45px; width: 94px;
             font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" Text="Fearless" Font-Size="25pt"></asp:Label>

        <asp:FileUpload ID="FileUpload1" runat="server" style="z-index: 1; left: 544px; top: 20px; position: absolute" ToolTip="XML file should be named &quot;CustomList.xml&quot;" />
        <asp:Button ID="Btn_Upload" runat="server" style="z-index: 1; left: 544px; top: 51px; position: absolute; width: 86px; margin-bottom: 0px;" Text="Upload" Height="20px" OnClick="Btn_Upload_Click" ToolTip="Upload file once it has been chosen above" />

        <asp:TextBox ID="Slider1" runat="server" style="z-index: 1; left: 184px; top: 98px; position: absolute; height: 30px; width: 304px" AutoPostBack="false" Value="3"></asp:TextBox>
        <asp:TextBox ID="Slider1_BoundControl" runat="server" style="z-index: 1; left: 503px; top: 98px; position: absolute; height: 17px; width: 21px"></asp:TextBox>

        <ajaxToolkit:SliderExtender ID="SliderExtender1" runat="server" TargetControlID="Slider1" BoundControlID="Slider1_BoundControl" Orientation="Horizontal"
            EnableHandleAnimation="true" Length="304" Minimum="1" Maximum="5" Steps="5"/>

        <asp:TextBox ID="Slider2" runat="server" style="z-index: 1; left: 184px; top: 141px; position: absolute; height: 30px; width: 304px" AutoPostBack="false" Value="3"></asp:TextBox>
        <asp:TextBox ID="Slider2_BoundControl" runat="server" style="z-index: 1; left: 503px; top: 135px; position: absolute; height: 17px; width: 21px"></asp:TextBox>

        <ajaxToolkit:SliderExtender ID="SliderExtender2" runat="server" TargetControlID="Slider2" BoundControlID="Slider2_BoundControl" Orientation="Horizontal"
            EnableHandleAnimation="true" Length="304" Minimum="1" Maximum="5" Steps="5"/>

        <asp:TextBox ID="Slider3" runat="server" style="z-index: 1; left: 184px; top: 184px; position: absolute; height: 30px; width: 304px" AutoPostBack="false" Value="3"></asp:TextBox>
        <asp:TextBox ID="Slider3_BoundControl" runat="server" style="z-index: 1; left: 503px; top: 178px; position: absolute; height: 17px; width: 21px"></asp:TextBox>

        <ajaxToolkit:SliderExtender ID="SliderExtender3" runat="server" TargetControlID="Slider3" BoundControlID="Slider3_BoundControl" Orientation="Horizontal"
            EnableHandleAnimation="true" Length="304" Minimum="1" Maximum="5" Steps="5"/>

        <asp:TextBox ID="Slider4" runat="server" style="z-index: 1; left: 184px; top: 227px; position: absolute; height: 30px; width: 304px" AutoPostBack="false" Value="3"></asp:TextBox>
        <asp:TextBox ID="Slider4_BoundControl" runat="server" style="z-index: 1; left: 503px; top: 221px; position: absolute; height: 17px; width: 21px"></asp:TextBox>

        <ajaxToolkit:SliderExtender ID="SliderExtender4" runat="server" TargetControlID="Slider4" BoundControlID="Slider4_BoundControl" Orientation="Horizontal"
            EnableHandleAnimation="true" Length="304" Minimum="1" Maximum="5" Steps="5"/>

        <asp:Image ID="Img_Poster1" runat="server" Height="180px" style="z-index: 1; left: 24px; top: 320px; position: absolute" Width="120px" ImageURL="Images/DefaultPoster.jpg"/>
        <asp:Image ID="Img_Poster2" runat="server" Height="180px" style="z-index: 1; left: 164px; top: 320px; position: absolute" Width="120px" ImageURL="Images/DefaultPoster.jpg"/>
        <asp:Image ID="Img_Poster3" runat="server" Height="180px" style="z-index: 1; left: 304px; top: 320px; position: absolute" Width="120px" ImageURL="Images/DefaultPoster.jpg"/>
        <asp:Image ID="Img_Poster4" runat="server" Height="180px" style="z-index: 1; left: 444px; top: 320px; position: absolute" Width="120px" ImageURL="Images/DefaultPoster.jpg"/>
        <asp:Image ID="Img_Poster5" runat="server" Height="180px" style="z-index: 1; left: 584px; top: 320px; position: absolute" Width="120px" ImageURL="Images/DefaultPoster.jpg"/>

        <asp:Label ID="Lbl_MovieName1" runat="server" style="z-index: 1; left: 32px; top: 514px; position: absolute; margin-left: auto; margin-right: auto; 
            text-align: center" Text="No Content" Width="100px" Height="40px"></asp:Label>
        <asp:Label ID="Lbl_MovieName2" runat="server" style="z-index: 1; left: 172px; top: 514px; position: absolute; margin-left: auto; margin-right: auto; 
            text-align: center" Text="No Content" Width="100px" Height="40px"></asp:Label>
        <asp:Label ID="Lbl_MovieName3" runat="server" style="z-index: 1; left: 312px; top: 514px; position: absolute; margin-left: auto; margin-right: auto; 
            text-align: center" Text="No Content" Width="100px" Height="40px"></asp:Label>
        <asp:Label ID="Lbl_MovieName4" runat="server" style="z-index: 1; left: 452px; top: 514px; position: absolute; margin-left: auto; margin-right: auto; 
            text-align: center" Text="No Content" Width="100px" Height="40px"></asp:Label>
        <asp:Label ID="Lbl_MovieName5" runat="server" style="z-index: 1; left: 592px; top: 514px; position: absolute; margin-left: auto; margin-right: auto; 
            text-align: center" Text="No Content" Width="100px" Height="40px"></asp:Label>
        <asp:Button ID="Btn_Update" runat="server" OnClick="Btn_Update_Click" style="z-index: 1; left: 164px; top: 274px; position: absolute; width: 141px; right: 1322px;" 
            Text="Show me the movies!" ToolTip="Updates movie selection using default file" />
        <asp:Button ID="Button_UpdateCustom" runat="server" style="z-index: 1; left: 375px; top: 274px; position: absolute; width: 140px" 
            Text="Use my custom list" ToolTip="Updates movie selection using custom file. Custom file can be uploaded above" OnClick="Button_UpdateCustom_Click" />
        <asp:Label ID="Lbl_CreatedBy" runat="server" Font-Size="Small" style="z-index: 1; left: 173px; top: 578px; position: absolute; width: 368px; color:dimgray" 
            Text="Created by Oscar Langford - oscarlangford@protonmail.com - 2018"></asp:Label>
    </form>
</body>
</html>
